# MDAccess for DOORS Next Snippets

This repository holds resources for a developer to get started on capabilities provided by MDAccess for DOORS Next.

See [https://www.sodiuswillert.com](https://www.sodiuswillert.com)

### Preparing your Eclipse Platform

Eclipse is the recommended platform to execute the snippets.

Here are the necessary steps to have a working environment for the snippets: 

1. In Eclipse, import the [com.sodius.oslc.app.dng.snippets](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets) project from the snippets repository. It is expected the code doesn't yet compile as the necessary libraries are not yet installed.
2. Select the **/com.sodius.oslc.app.dng.snippets/target/snippets.target** file and right-click **Open With > Target Editor**
6. Click **Set as Active Platform** on top right

At this stage the code should successfully compile in **com.sodius.oslc.app.dng.snippets** project.

### Obtaining a License

MDAccess for DOORS Next is license protected. Contact us to get a SodiusWillert license file to run the snippets

### Executing Snippets

This repository contains snippets demonstrating how to use MDAccess classes to query and update resources in a [DOORS Next](https://jazz.net/products/requirements-management-doors-next/) application.

Those snippets are located here: [src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/)

The Javadoc of each snippet details the expected program arguments and Java VM arguments for the snippet to run properly.

You may refer to MDAccess for DOORS Next [Developer Guide](https://help.sodius.cloud/help/topic/com.sodius.oslc.app.dng.doc/html/overview.html) for more details.

### Snippet List

Here are the snippets you may want to start with:

* [ListProjectsSnippet](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/ListProjectsSnippet.java): list available projects in DOORS Next
* [ListConfigurations](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/ListConfigurations.java): list DOORS Next local configurations and GCM global configurations
* [ListFoldersSnippet](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/ListFoldersSnippet.java): list folder hierarchy in a project
* [ListArtifactsSnippet](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/ListArtifactsSnippet.java): list modules and requirements within a folder
* [ReadModuleContentSnippet](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/ReadModuleContentSnippet.java): read the content (requirements) of a given module
* [UpdateRequirementSnippet](https://bitbucket.org/sodius/public.mdaccess.dng.snippets/src/master/src/com.sodius.oslc.app.dng.snippets/src/com/sodius/oslc/app/dng/snippets/UpdateRequirementSnippet.java): update the title of one requirement

The snippets package contains more snippets, make sure to review all of them to see all capabilities being demonstrated.