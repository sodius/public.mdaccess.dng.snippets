package com.sodius.oslc.app.dng.snippets;

import java.net.URI;

import com.sodius.oslc.app.dng.model.DngRequirement;
import com.sodius.oslc.app.dng.requests.GetRequirement;
import com.sodius.oslc.app.dng.requests.UpdateRequirement;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.ResourceResponse;

/**
 * Updates the title of the specified DOORS Next requirement.
 * </p>
 *
 * <p>
 * Required Program arguments:
 * </p>
 * <ul>
 * <li><code>args[0]</code>: new title to assign to the DOORS Next requirement</li>
 * </ul>
 * <p>
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.requirement</code> - URL of the DOORS Next requirement,
 * which is obtained by right-clicking "Share Link to Artifact.." on a requirement in DOORS Next..</li>
 * </ul>
 */
public class UpdateRequirementSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Expects a requirement title as command line argument");
        }
        String title = args[0];
        new UpdateRequirementSnippet(title).call();
    }

    private final String title;

    private UpdateRequirementSnippet(String title) {
        this.title = title;
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // load existing requirement
        System.out.println("Reading requirement...");
        URI requirementUri = URI.create(getRequiredProperty("dng.requirement"));
        ResourceResponse<DngRequirement> response = new GetRequirement(client, requirementUri).call();
        DngRequirement requirement = response.getEntity();
        String eTag = response.getETag();
        System.out.println("Requirement: [" + requirement.getIdentifier() + "] " + requirement.getTitle());
        System.out.println("ETag: " + eTag);

        // change title in memory
        requirement.setTitle(title);

        // update remote requirement
        System.out.println("Updating requirement title...");
        ResourceResponse<Void> updatedResponse = new UpdateRequirement(client, requirementUri, eTag, requirement).call();
        System.out.println("New ETag: " + updatedResponse.getETag());

        System.out.println("Done.");
    }

}
