package com.sodius.oslc.app.dng.snippets;

import java.lang.reflect.InvocationTargetException;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import javax.security.auth.login.LoginException;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.window.Window;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.sodius.oslc.app.dng.model.DngRootServices;
import com.sodius.oslc.app.dng.snippets.utils.TitleComparator;
import com.sodius.oslc.client.ClientAuthenticationException;
import com.sodius.oslc.client.ClientWebException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.client.requests.GetServiceProviderCatalog;
import com.sodius.oslc.client.ui.util.OslcSafeRunnable;

/**
 * Shows a connection dialog allowing a user to connect to DOORS Next and display the list of projects.
 *
 * <p>
 * This snippet doesn't require any System property to execute.
 */
public class ShowConnectionDialogSnippet implements Callable<Void> {

    public static void main(String[] args) throws Exception {
        new ShowConnectionDialogSnippet().call();
    }

    @Override
    public Void call() throws Exception {

        // open login dialog
        final Display display = Display.getDefault();
        LoginDialog dialog = new LoginDialog(null);
        if (dialog.open() == Window.OK) {
            String server = dialog.getURL();
            Credentials credentials = dialog.getCredentials();

            // logs-in DOORS Next
            SafeRunnable.run(new LoginRunnable(server, credentials, display));
        }

        return null;
    }

    /*
     * Connects to DOORS Next and show the project titles in a dialog.
     */
    private static final class LoginRunnable extends OslcSafeRunnable {
        private final String server;
        private final Credentials credentials;
        private final Display display;

        private LoginRunnable(String server, Credentials credentials, Display display) {
            this.server = server;
            this.display = display;
            this.credentials = credentials;
        }

        @Override
        public void run() throws Exception {

            // run the process in a progress dialog
            new ProgressMonitorDialog(null).run(true /* fork in separate thread */, false /* not cancellable */, new IRunnableWithProgress() {

                @Override
                public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
                    monitor.beginTask("Connecting to " + server, IProgressMonitor.UNKNOWN);

                    // read root services
                    OslcClient client = OslcClients.jazzForm(credentials).create();
                    DngRootServices rootservices = new GetRootServices<>(getRootServicesLocation(server), DngRootServices.class).call();
                    if (rootservices.getRmCatalog() == null) {
                        throw new IllegalArgumentException("Root services does not contain a RM catalog");
                    }

                    // read catalog
                    ServiceProviderCatalog catalog = new GetServiceProviderCatalog(client, rootservices.getRmCatalog()).get();

                    // sort projects by title
                    List<ServiceProvider> providers = new ArrayList<>(Arrays.asList(catalog.getServiceProviders()));
                    Collections.sort(providers, new TitleComparator());

                    // print the projects
                    final StringBuilder message = new StringBuilder("Successfully connected to DOORS Next.\n\n");
                    message.append("Projects:\n");
                    for (ServiceProvider provider : providers) {
                        message.append("- " + provider.getTitle() + "\n");
                    }

                    // display the projects in the UI (re-syncing with the UI thread)
                    display.asyncExec(() -> MessageDialog.openInformation(null, "Connection Successful", message.toString()));
                }
            });
        }

        @Override
        public void handleException(Throwable e) {
            if (e instanceof InvocationTargetException) {
                handleException(((InvocationTargetException) e).getTargetException());
            } else {
                Throwable cause = Throwables.getRootCause(e);
                if ((cause instanceof UnknownHostException) || (cause instanceof ConnectException) || (cause instanceof LoginException)
                        || (cause instanceof ClientAuthenticationException) || (cause instanceof ClientWebException)) {
                    super.handleException(e);
                } else {
                    MessageDialog.openError(null, "Error", e.toString());
                }
            }
        }
    }

    /*
     * A dialog to get the DOORS Next server and user identity to use.
     */
    private static class LoginDialog extends StatusDialog {

        private Text uriField;
        private String uri;
        private Text usernameField;
        private Text passwordField;
        private Credentials credentials;

        LoginDialog(final Shell parentShell) {
            super(parentShell);
            setShellStyle(getShellStyle() | SWT.RESIZE);
            setTitle("DOORS Next Connection");
            setStatusLineAboveButtons(true);
        }

        @Override
        protected Control createDialogArea(final Composite parent) {
            final Composite top = new Composite(parent, SWT.NONE);
            GridLayout layout = new GridLayout();
            layout.numColumns = 2;

            top.setLayout(layout);
            top.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            final Composite imageComposite = new Composite(top, SWT.NONE);
            layout = new GridLayout();
            imageComposite.setLayout(layout);
            imageComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

            final Composite main = new Composite(top, SWT.NONE);
            layout = new GridLayout();
            layout.numColumns = 3;
            main.setLayout(layout);
            main.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            final Label messageLabel = new Label(main, SWT.WRAP);
            messageLabel.setText("Enter the server address and credentials to connect to DOORS Next:");
            GridData data = new GridData(GridData.FILL_HORIZONTAL | GridData.GRAB_HORIZONTAL);
            data.horizontalSpan = 3;
            data.widthHint = 300;
            messageLabel.setLayoutData(data);

            createFieldArea(main);
            Dialog.applyDialogFont(parent);

            uriField.setText("https://myserver:9443/rm");

            return main;
        }

        private void createFieldArea(final Composite parent) {
            uriField = createText(parent, "URL", SWT.BORDER);
            usernameField = createText(parent, "User ID", SWT.BORDER);
            passwordField = createText(parent, "Password", SWT.BORDER | SWT.PASSWORD);
        }

        private Label createLabel(Composite parent, String label) {
            Label labelWidget = new Label(parent, SWT.NONE);
            labelWidget.setText(label);
            return labelWidget;
        }

        private Text createText(final Composite parent, final String label, final int style) {
            createLabel(parent, label);

            final Text text = new Text(parent, style);
            final GridData data = new GridData(GridData.FILL_HORIZONTAL);
            data.horizontalSpan = 2;
            data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.ENTRY_FIELD_WIDTH);
            text.setLayoutData(data);

            text.addModifyListener(event -> updateStatus(computeStatus()));

            text.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(final FocusEvent e) {
                    text.selectAll();
                }
            });

            return text;
        }

        private String getURL() {
            return uri;
        }

        private Credentials getCredentials() {
            return credentials;
        }

        @Override
        protected void okPressed() {
            credentials = new UsernamePasswordCredentials(usernameField.getText(), passwordField.getText());
            uri = uriField.getText();
            super.okPressed();
        }

        private IStatus computeStatus() {

            // server
            if (uriField.getText().length() == 0) {
                return new Status(IStatus.ERROR, "com.sodius.oslc.app.dng.snippets", ""); //$NON-NLS-1$
            }

            // user
            if (usernameField.getText().length() == 0) {
                return new Status(IStatus.ERROR, "com.sodius.oslc.app.dng.snippets", ""); //$NON-NLS-1$
            }

            // password
            if (passwordField.getText().length() == 0) {
                return new Status(IStatus.ERROR, "com.sodius.oslc.app.dng.snippets", ""); //$NON-NLS-1$
            }

            return new Status(IStatus.OK, "com.sodius.oslc.app.dng.snippets", ""); //$NON-NLS-1$
        }
    }

    private static URI getRootServicesLocation(String server) throws IllegalArgumentException {

        // server
        if (Strings.isNullOrEmpty(server)) {
            throw new IllegalArgumentException(
                    "The URL is not valid for DOORS Next connections. Valid URL syntax:\nhttps://[dngServerName][.domain][:port]/rm");
        }

        // verify this is a URI
        try {
            URI uri = new URI(server);

            // scheme
            if (Strings.isNullOrEmpty(uri.getScheme())) {
                throw new URISyntaxException(server, "the scheme is not set");
            }
            if (!"https".equalsIgnoreCase(uri.getScheme()) && !"http".equalsIgnoreCase(uri.getScheme())) {
                throw new URISyntaxException(server, "unsupported scheme");
            }

            // host
            if (Strings.isNullOrEmpty(uri.getHost())) {
                throw new URISyntaxException(server, "the host name is not set");
            }

            // path
            String path = getRootServicesPath(server, uri.getPath());

            return new URI(uri.getScheme(), null /* userInfo */, uri.getHost(), uri.getPort(), path, null /* query */, null /* fragment */);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(
                    "The URL is not valid for DOORS Next connections. Valid URL syntax:\nhttps://[dngServerName][.domain][:port]/rm");
        }
    }

    private static String getRootServicesPath(String location, String path) throws URISyntaxException {

        // count the slash characters in the path
        int slashCount = CharMatcher.is('/').countIn(path);

        // "https://server:9443" ?
        if (slashCount == 0) {
            return path + "/rm/rootservices";
        }

        // "https://server:9443/xxx" ?
        else if (slashCount == 1) {

            // "https://server:9443/" ?
            if (path.length() == 1) {
                return path + "rm/rootservices";
            }

            // "https://server:9443/rm"
            else {
                return path + "/rootservices";
            }
        }

        // "https://server:9443/rm/xxx" ?
        else if (slashCount == 2) {

            // "https://server:9443/rm/" ?
            if (path.endsWith("/")) {
                return path + "rootservices";
            }

            // "https://server:9443/rm/rootservices" ?
            else if (path.endsWith("/rootservices")) {
                return path;
            }

            else {
                throw new URISyntaxException(location, "invalid path");
            }
        }

        else {
            throw new URISyntaxException(location, "invalid path");
        }
    }
}
