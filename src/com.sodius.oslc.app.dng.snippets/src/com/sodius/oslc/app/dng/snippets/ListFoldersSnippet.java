package com.sodius.oslc.app.dng.snippets;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.lyo.oslc4j.core.model.QueryCapability;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.dng.model.Folder;
import com.sodius.oslc.app.dng.model.JazzRmNavigation;
import com.sodius.oslc.app.dng.snippets.utils.TitleComparator;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.OslcRm;

/**
 * Prints out the full hierarchy of folders contained in a project.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.project</code> - URL of the DOORS Next project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ListFoldersSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListFoldersSnippet().call();
    }

    @Override
    protected void run(OslcClient client) {

        // load project
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("dng.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();
        System.out.println("Project: " + provider.getTitle() + " <" + providerLocation + '>');

        // lookup capability to query folders
        Service service = OslcCore.Finder.forServices(provider).domain(URI.create(OslcRm.DOMAIN)).findFirst();
        QueryCapability query = OslcCore.Finder.forQueryCapabilities(service).resourceType(URI.create(JazzRmNavigation.TYPE_FOLDER)).findFirst();

        URI configuration = OslcConfig.getContext(providerLocation);
        System.out.println("Folders:");
        run(client, query.getQueryBase(), configuration, "");
    }

    private void run(OslcClient client, URI uri, URI configuration, String prefix) {

        // query folders
        for (Folder folder : queryFolders(client, uri, configuration)) {
            System.out.println(prefix + "- " + folder.getTitle() + " <" + folder.getAbout() + '>');

            // query sub folders
            run(client, folder.getSubfolders(), configuration, prefix + "  ");
        }
    }

    private List<Folder> queryFolders(OslcClient client, URI uri, URI configuration) {

        // query folders
        URI versionedURI = configuration == null ? uri : OslcConfig.addContext(uri, configuration);
        Collection<Folder> folders = new GetResources<>(client, versionedURI, Folder.class).get();

        // sort folders by title
        List<Folder> result = new ArrayList<>(folders);
        Collections.sort(result, new TitleComparator());
        return result;
    }

}
