package com.sodius.oslc.app.dng.snippets;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;

import com.sodius.oslc.app.dng.model.DngRootServices;
import com.sodius.oslc.app.dng.requests.DngValidateConnection;
import com.sodius.oslc.app.dng.snippets.utils.TitleComparator;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.client.requests.GetServiceProviderCatalog;

/**
 * Validates a given Root Services location and loads the RM catalog.
 * It notably ensures the server is accessible, the provided credentials allows connecting and that the server is indeed a DOORS Next application.
 * It then reads the Root Services document to determine the location of the Service Provider Catalog.
 * It finally loads the catalog to list the available projects.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.rootservices</code> - URL of the DOORS Next Root Services
 * (e.g. <code>https://myserver.mydomain.com:9443/rm/rootservices</code></li>
 * </ul>
 */
public class ListProjectsSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListProjectsSnippet().call();
    }

    @Override
    protected void run(OslcClient client) {

        // validate repository
        System.out.println("Validating connection to DOORS Next...");
        URI rootServicesLocation = URI.create(getRequiredProperty("dng.rootservices"));
        IStatus status = new DngValidateConnection(client, rootServicesLocation).call();
        if (!status.isOK()) {
            System.out.println("Connection failure!");
            System.out.println(status.getMessage());
            return;
        }

        // read root services
        System.out.println("Reading Root Services...");
        DngRootServices rootServices = new GetRootServices<>(rootServicesLocation, DngRootServices.class).call();

        // read catalog
        System.out.println("Reading Catalog...");
        ServiceProviderCatalog catalog = new GetServiceProviderCatalog(client, rootServices.getRmCatalog()).get();

        // sort projects by title
        List<ServiceProvider> providers = new ArrayList<>(Arrays.asList(catalog.getServiceProviders()));
        Collections.sort(providers, new TitleComparator());

        // print out the projects
        System.out.println("Catalog has " + providers.size() + " projects:");
        for (ServiceProvider provider : providers) {
            System.out.println(" - " + provider.getTitle() + ": <" + provider.getAbout() + '>');
        }
    }

}
