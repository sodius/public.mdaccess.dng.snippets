package com.sodius.oslc.app.dng.snippets;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.lyo.oslc4j.core.model.QueryCapability;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.dng.model.Folder;
import com.sodius.oslc.app.dng.requests.GetFolder;
import com.sodius.oslc.app.dng.snippets.utils.Folders;
import com.sodius.oslc.app.dng.snippets.utils.TitleComparator;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.OslcRm;
import com.sodius.oslc.domain.rm.model.Requirement;
import com.sodius.oslc.domain.rm.model.RequirementCollection;

/**
 * Prints out the artifacts (requirements, collections, modules) contained in a folder.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.folder</code> - URL of the DOORS Next folder, which can be obtained by executing ListFoldersSnippet.
 * The folder URI is also available by right-clicking "Share Link to Folder.." on a folder in DOORS Next.
 * </li>
 * </ul>
 */
public class ListArtifactsSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListArtifactsSnippet().call();
    }

    @Override
    protected void run(OslcClient client) throws UnsupportedEncodingException {

        // read folder
        System.out.println("Reading Folder...");
        URI folderLocation = Folders.format(URI.create(getRequiredProperty("dng.folder")));
        URI configuration = OslcConfig.getContext(folderLocation);
        Folder folder = new GetFolder(client, folderLocation).get();

        // loading project
        System.out.println("Reading Project...");
        URI providerLocation = configuration == null ? folder.getServiceProvider()
                : OslcConfig.addContext(folder.getServiceProvider(), configuration);
        ServiceProvider provider = new GetServiceProvider(client, folder.getServiceProvider()).get();

        System.out.println("Project: " + provider.getTitle() + " <" + providerLocation + '>');
        System.out.println("Folder: " + folder.getTitle() + " <" + folderLocation + '>');

        // lookup capability to query requirements
        Service service = OslcCore.Finder.forServices(provider).domain(URI.create(OslcRm.DOMAIN)).findFirst();
        QueryCapability queryCapability = OslcCore.Finder.forQueryCapabilities(service).resourceType(URI.create(OslcRm.TYPE_REQUIREMENT)).findFirst();

        // run the query
        URI query = getQuery(queryCapability, OslcConfig.removeContext(folderLocation));
        run(client, query, configuration);
    }

    /*
     * Determines the actual query to perform on requirements.
     *
     * Some possible queries are introduced in the article ("Using the query capabilities" section):
     *
     * Using OSLC capabilities in the Requirements Management application
     * https://jazz.net/library/article/1197
     */
    private URI getQuery(QueryCapability query, URI folderLocation) throws UnsupportedEncodingException {
        StringBuilder buffer = new StringBuilder();

        // OSLC query base
        buffer.append(query.getQueryBase());

        // OSLC prefixes
        buffer.append("&oslc.prefix=");
        buffer.append(URLEncoder.encode("oslc=<http://open-services.net/ns/core#>", "UTF-8"));
        buffer.append(',');
        buffer.append(URLEncoder.encode("dcterms=<http://purl.org/dc/terms/>", "UTF-8"));
        buffer.append(',');
        buffer.append(URLEncoder.encode("nav=<http://com.ibm.rdm/navigation#>", "UTF-8"));

        // 'select' clause
        buffer.append("&oslc.select=dcterms:title");

        // 'where' clause on parent folder
        buffer.append("&oslc.where=");
        buffer.append(URLEncoder.encode("nav:parent=<" + folderLocation.toString() + '>', "UTF-8"));

        return URI.create(buffer.toString());
    }

    private void run(OslcClient client, URI uri, URI configuration) {
        URI versionedURI = configuration == null ? uri : OslcConfig.addContext(uri, configuration);

        // query modules/collections
        Collection<RequirementCollection> modules = new GetResources<>(client, versionedURI, RequirementCollection.class).get();
        listRequirements("Modules", modules);

        // query requirements
        Collection<Requirement> requirements = new GetResources<>(client, versionedURI, Requirement.class).get();
        listRequirements("Requirements", requirements);
    }

    private void listRequirements(String category, Collection<? extends Requirement> requirements) {
        System.out.println(category + " (" + requirements.size() + ')');

        // sort requirements by title
        List<Requirement> sortedRequirements = new ArrayList<>(requirements);
        Collections.sort(sortedRequirements, new TitleComparator());

        for (Requirement requirement : sortedRequirements) {
            System.out.println("- " + requirement.getTitle() + " <" + requirement.getAbout() + '>');
        }
    }
}
