package com.sodius.oslc.app.dng.snippets;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.sodius.oslc.app.dng.model.DngRequirement;
import com.sodius.oslc.app.dng.model.Module;
import com.sodius.oslc.app.dng.model.ModuleContext;
import com.sodius.oslc.app.dng.model.ModuleContextBinding;
import com.sodius.oslc.app.dng.requests.GetModule;
import com.sodius.oslc.app.dng.requests.GetModuleContext;
import com.sodius.oslc.app.dng.requests.GetRequirement;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;
import com.sodius.oslc.client.requests.ResourceResponse;

/**
 * Loads the OSLC resource describing the specified DOORS Next module. Then loads in parallel all requirement resources referenced by the module.
 * Last, loads the structure of the module, meaning the exact ordered hierarchy of requirements.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.module</code> - URL of the DOORS Next module,
 * which is obtained by right-clicking "Share Link to Artifact.." on a module in DOORS Next..</li>
 * </ul>
 */
public class ReadModuleContentSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ReadModuleContentSnippet().call();
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // read module
        System.out.println("Reading module...");
        URI moduleUri = URI.create(getRequiredProperty("dng.module"));
        Module module = new GetModule(client, moduleUri).get();
        System.out.println("URL: <" + moduleUri + '>');
        System.out.println("Title: " + module.getTitle());
        System.out.println("Identifier: " + module.getIdentifier());
        System.out.println("Folder: <" + module.getParentFolder() + '>');
        System.out.println();

        // read requirements
        System.out.println("Reading " + module.getUses().length + " requirements...");
        List<DngRequirement> requirements = readRequirements(client, module);
        for (DngRequirement requirement : requirements) {
            System.out.println("- [" + requirement.getIdentifier() + "] " + requirement.getTitle() + ": " + requirement.getPrimaryText());
        }
        System.out.println();

        // read structure
        System.out.println("Reading module structure...");
        ModuleContext context = new GetModuleContext(client, moduleUri).call();
        printHierarchy(context, context.getRoots(), "");
    }

    private void printHierarchy(ModuleContext context, List<ModuleContextBinding> bindings, String prefix) {
        for (ModuleContextBinding binding : bindings) {
            System.out.println(prefix + "[" + binding.getIdentifier() + "] " + binding.getSection() + " " + binding.getTitle());
            printHierarchy(context, context.getChildren(binding), prefix + "  ");
        }

    }

    private List<DngRequirement> readRequirements(OslcClient client, Module module) throws Exception {

        // create an OSLC client that allows parallel request executions (one per Thread)
        OslcClient concurrentClient = OslcClients.concurrent(client);

        // request to read requirements in parallel
        Collection<Future<ResourceResponse<DngRequirement>>> futures = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(8);
        for (URI uri : module.getUses()) {
            futures.add(executor.submit(new GetRequirement(concurrentClient, uri)));
        }

        // wait for at most 2 minutes
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.MINUTES);

        // return read requirements
        List<DngRequirement> result = new ArrayList<>();
        for (Future<ResourceResponse<DngRequirement>> future : futures) {
            ResourceResponse<DngRequirement> response = future.get();
            if (response != null) {
                result.add(response.getEntity());
            }
        }
        return result;
    }
}
