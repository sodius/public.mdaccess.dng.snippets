package com.sodius.oslc.app.dng.snippets.utils;

import java.net.URI;
import java.net.URISyntaxException;

import com.sodius.oslc.domain.config.model.OslcConfig;

public class Folders {

    /*
     * A folder web link is available by right-clicking "Share Link to Folder.." on a folder in DOORS Next.
     *
     * Until DOORS Next 6.0.6:
     * A folder web link provides project/stream/folder information, e.g.:
     *
     * https://server:9443/rm/web#action=com.ibm.rdm.web.pages.showProjectDashboard&componentURI=https%3A%2F%2Fserver%3A9443%2Frm%2Frm-projects%
     * 2F_uOqKcFvgEeehxPoV1tqaLQ%2Fcomponents%2F_vIxpUFvgEeehxPoV1tqaLQ&folderId=_05d_o4QvEeec2YUWGMa17w&vvc.configuration=https%3A%2F%2Fserver%
     * 3A9443%2Frm%2Fcm%2Fstream%2F_vbi0WFvgEeehxPoV1tqaLQ
     *
     * Since DOORS Next 6.0.6.1:
     * A folder web link provides the folder ID and the config context, e.g.:
     * https://server:9443/rm/folders/FR_qAbEQEwoEeqjsewwsoS1JQ
     * ?oslc_config.context=https%3A%2F%2Fserver%3A9443%2Frm%2Fcm%2Fstream%2F_nykFkEwoEeqjsewwsoS1JQ
     */
    public static URI format(URI folderLink) {
        try {
            // an OSLC Configuration is set on the folder?
            if ((folderLink.getFragment() == null) || (OslcConfig.getContext(folderLink) != null)) {

                // This is DOORS Next >= 6.0.6.1, no need to format the folder URI
                return folderLink;
            }

            else {
                // This is DOORS Next < 6.0.6.1, need to split the URL parameters to identify the folder and configuration
                String[] parameters = folderLink.getFragment().split("&"); //$NON-NLS-1$

                // lookup the configuration
                URI configuration;
                try {
                    configuration = URI.create(getRequiredParameter(parameters, "vvc.configuration")); // local configuration //$NON-NLS-1$
                } catch (IllegalArgumentException e) {
                    configuration = URI.create(getRequiredParameter(parameters, "oslc.configuration")); // global configuration //$NON-NLS-1$
                }

                // lookup the folder path
                String contextPath = getContextPath(folderLink);
                String folderId = getRequiredParameter(parameters, "folderId"); //$NON-NLS-1$
                String folderPath = contextPath + "/folders/" + folderId; //$NON-NLS-1$
                URI folder = new URI(folderLink.getScheme(), null, folderLink.getHost(), folderLink.getPort(), folderPath, null, null);

                return OslcConfig.addContext(folder, configuration);
            }

        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static String getRequiredParameter(String[] parameters, String name) {
        for (String parameter : parameters) {
            if (parameter.startsWith(name + '=')) {
                return parameter.substring(name.length() + 1);
            }
        }

        throw new IllegalArgumentException("Missing query parameter: " + name); //$NON-NLS-1$
    }

    /*
     * https://server:9443/rm/oslc_rm/_uOqKcFvgEeehxPoV1tqaLQ/services.xml
     * ->
     * /rm
     */
    private static String getContextPath(URI uri) {
        String path = uri.getPath();
        int i = path.indexOf('/', 1); // don't stop on first slash!
        if (i > 0) {
            return path.substring(0, i);
        } else {
            return path;
        }
    }

    private Folders() {
    }
}
