package com.sodius.oslc.app.dng.snippets;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.lyo.oslc4j.core.model.Dialog;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.swt.widgets.Shell;

import com.sodius.oslc.app.jazz.model.GlobalConfigurationAware;
import com.sodius.oslc.app.jazz.requests.GetGlobalConfigurationSelectionDialog;
import com.sodius.oslc.app.jazz.requests.GetLocalConfigurationSelectionDialog;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.client.ui.browser.Browsers;
import com.sodius.oslc.client.ui.dialogs.ResourceSelectionDialog;
import com.sodius.oslc.core.util.Uris;

/**
 * Shows a selection dialog to select either a local configuration of a DOORS Next project or a global configuration.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.project</code> - URL of the DOORS Next project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ShowConfigurationDialogSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ShowConfigurationDialogSnippet().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read provider
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("dng.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();

        // determine whether configuration management is enabled for this project
        System.out.println("Project: " + provider.getTitle() + " <" + providerLocation + '>');

        // no configuration?
        if (!isConfigurationEnabled(provider)) {
            String message = "The following project is not configuration enabled:\n" + provider.getTitle();
            MessageDialog.openInformation(new Shell(), "Configuration Selection", message);
            return;
        }

        // determine the RM root services location
        URI rootServicesLocation = getRootServicesLocation(provider.getAbout());

        // find selection dialogs
        System.out.println("Analyzing configurations...");
        Optional<Dialog> globalDialog = new GetGlobalConfigurationSelectionDialog(client, rootServicesLocation).call();
        Optional<Dialog> localDialog = new GetLocalConfigurationSelectionDialog(client, rootServicesLocation, providerLocation).call();

        // create selection dialog
        // @formatter:off
        ResourceSelectionDialog selectionDialog = ResourceSelectionDialog.forConfiguration(null,
                globalDialog.orElse(null),
                localDialog.orElse(null));
        // @formatter:on
        selectionDialog.create();
        selectionDialog.getShell().setText("Configuration Context Selection");
        selectionDialog.setTitle("Select the Configuration Context");
        selectionDialog.setMessage("Choose the stream which determines the state of artifacts to use.");

        // configures the browser within the selection dialog to use credential provided as System properties,
        // avoiding the user to manually enter them.
        Credentials credentials = new UsernamePasswordCredentials(getRequiredProperty("dng.user"), getRequiredProperty("dng.password"));
        Browsers.setJazzCredentials(selectionDialog.getBrowser(), credentials);

        // show the dialog
        if (selectionDialog.open() == Window.OK) {

            // prints out the selected configuration
            Link selectedConfiguration = selectionDialog.getResult().get(0);
            System.out.println("Selected configuration: ");
            System.out.println("  - Title: " + selectedConfiguration.getLabel());
            System.out.println("  - URI:   " + selectedConfiguration.getValue());
        }
    }

    /*
     * Determine whether configuration management is enabled for this project
     */
    private static boolean isConfigurationEnabled(ServiceProvider provider) {
        return GlobalConfigurationAware.of(provider) == GlobalConfigurationAware.YES;
    }

    /*
     * Determine the URI of the root services given the URI of a resource within the application.
     */
    private static URI getRootServicesLocation(URI uri) {
        try {
            return new URI(Uris.getContext(uri) + "/rootservices");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
