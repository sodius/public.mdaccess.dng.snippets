package com.sodius.oslc.app.dng.snippets;

import java.net.URI;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.eclipse.jface.window.Window;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.client.ui.browser.Browsers;
import com.sodius.oslc.client.ui.dialogs.ResourceSelectionDialog;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.domain.rm.model.OslcRm;

/**
 * Shows a selection dialog to either select an existing requirement or create a new one in DOORS Next.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.project</code> - URL of the DOORS Next project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ShowRequirementSelectionDialogSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ShowRequirementSelectionDialogSnippet().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read provider
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("dng.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();

        // find RM service
        Service service = OslcCore.Finder.forServices(provider).domain(URI.create(OslcRm.DOMAIN)).findFirst();

        // create selection dialog
        ResourceSelectionDialog selectionDialog = ResourceSelectionDialog.forService(null, service, URI.create(OslcRm.TYPE_REQUIREMENT));
        selectionDialog.create();
        selectionDialog.getShell().setText("Requirement Selection");
        selectionDialog.setTitle("Select the Configuration Context");
        selectionDialog.setMessage("Choose the stream which determines the state of artifacts to use.");

        // configures the browser within the selection dialog to use credential provided as System properties,
        // avoiding the user to manually enter them.
        Credentials credentials = new UsernamePasswordCredentials(getRequiredProperty("dng.user"), getRequiredProperty("dng.password"));
        Browsers.setJazzCredentials(selectionDialog.getBrowser(), credentials);

        // show the dialog
        if (selectionDialog.open() == Window.OK) {

            // prints out the selected configuration
            Link selectedRequirement = selectionDialog.getResult().get(0);
            System.out.println("Selected requirement: ");
            System.out.println("  - Title: " + selectedRequirement.getLabel());
            System.out.println("  - URI:   " + selectedRequirement.getValue());
        }
    }
}
