package com.sodius.oslc.app.dng.snippets;

import java.util.concurrent.Callable;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;

import com.sodius.oslc.client.ClientWebException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;

/**
 * Base code to execute a snippet demonstrating MDAccess for DOORS Next features.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * </ul>
 */
public abstract class AbstractSnippet implements Callable<Void> {

    /**
     * Creates an OSLC client to connect to DOORS Next and calls the <code>run()</code> method.
     *
     * @see #run(OslcClient)
     */
    @Override
    public Void call() throws Exception {
        try {
            OslcClient client = createDoorsNextClient();
            run(client);
        } catch (ClientWebException e) {
            System.err.println("Failed to execute " + e.getRequest().getURI());
            System.err.println("Status " + e.getResponse().getStatusCode() + " - " + e.getResponse().getMessage());
            System.err.println("Entity: " + e.getResponse().getEntity(String.class));
        }

        return null;
    }

    /**
     * Sub-classes must implement what's to do when an OSLC client is available for DOORS Next.
     */
    protected abstract void run(OslcClient client) throws Exception;

    /**
     * Creates an OSLC client to connect to DOORS Next.
     * The client uses Jazz FORM authentication, emulating the user authentication that usually happens in a browser.
     * Client uses credentials specified with <code>dng.user</code> and <code>dng.password</code> Java virtual machine arguments.
     */
    private static OslcClient createDoorsNextClient() {
        Credentials credentials = new UsernamePasswordCredentials(getRequiredProperty("dng.user"), getRequiredProperty("dng.password"));
        return OslcClients.jazzForm(credentials).create();
    }

    static String getRequiredProperty(String name) {
        String value = System.getProperty(name);
        if ((value == null) || value.isEmpty()) {
            throw new IllegalArgumentException("Missing required Java virtual machine argument: " + name);
        } else {
            return value;
        }
    }
}
