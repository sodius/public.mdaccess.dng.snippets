package com.sodius.oslc.app.dng.snippets;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.eclipse.lyo.oslc4j.core.model.CreationFactory;
import org.eclipse.lyo.oslc4j.core.model.QueryCapability;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;

import com.sodius.oslc.app.jazz.model.Friend;
import com.sodius.oslc.app.jazz.model.GlobalConfigurationAware;
import com.sodius.oslc.app.jazz.requests.GetGlobalConfigurationFriend;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.client.requests.GetServiceProviderCatalog;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.core.model.RootServices;
import com.sodius.oslc.core.util.Uris;
import com.sodius.oslc.domain.config.model.Component;
import com.sodius.oslc.domain.config.model.Configuration;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.config.requests.GetComponent;
import com.sodius.oslc.domain.config.requests.GetConfiguration;

/**
 * Prints out the local configurations of a DOORS Next project and all global configurations.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.project</code> - URL of the DOORS Next project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ListConfigurations extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListConfigurations().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read provider
        System.out.println("Reading Project...");
        URI rmProviderLocation = URI.create(getRequiredProperty("dng.project"));
        ServiceProvider rmProvider = new GetServiceProvider(client, rmProviderLocation).get();

        // determine whether configuration management is enabled for this project
        boolean configurationEnabled = isConfigurationEnabled(rmProvider);
        System.out.println("URL: <" + rmProviderLocation + '>');
        System.out.println("Title: " + rmProvider.getTitle());
        System.out.println("Configuration Enabled: " + configurationEnabled);
        System.out.println();

        if (configurationEnabled) {

            // load the RM root services
            RootServices rootServices = new GetRootServices<>(getRootServicesLocation(rmProvider.getAbout()), RootServices.class).call();

            // global configurations
            new ListGlobalConfigurations(rootServices).run(client);

            System.out.println();

            // local configurations
            new ListLocalConfigurations(rootServices, rmProvider).run(client);
        }
    }

    /*
     * Determine whether configuration management is enabled for this project
     */
    private static boolean isConfigurationEnabled(ServiceProvider provider) {
        return GlobalConfigurationAware.of(provider) == GlobalConfigurationAware.YES;
    }

    /*
     * Determine the URI of the root services given the URI of a resource within the application.
     */
    private static URI getRootServicesLocation(URI uri) {
        try {
            return new URI(Uris.getContext(uri) + "/rootservices");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static void printConfiguration(Configuration configuration) {
        String type = "Configuration";
        if (configuration.getTypes().contains(URI.create(OslcConfig.TYPE_STREAM))) {
            type = "Stream";
        } else if (configuration.getTypes().contains(URI.create(OslcConfig.TYPE_BASELINE))) {
            type = "Baseline";
        }

        System.out.println("    [" + type + "] " + configuration.getTitle() + " <" + configuration.getAbout() + '>');
    }

    /*
     * Prints the components and configurations part of the specified RM project.
     */
    private static class ListLocalConfigurations extends AbstractSnippet {

        private final RootServices rmRootServices;
        private final ServiceProvider rmProvider;

        private ListLocalConfigurations(RootServices rmRootServices, ServiceProvider rmProvider) {
            this.rmRootServices = rmRootServices;
            this.rmProvider = rmProvider;
        }

        @Override
        protected void run(OslcClient client) {
            System.out.println("Loading local configurations...");

            // load the configuration provider
            ServiceProvider configProvider = loadConfigProvider(client, rmRootServices);

            // loop on components
            for (Component component : loadComponents(client, configProvider)) {

                // list configurations of the component
                Collection<URI> configurationLocations = new GetResources<>(client, component.getConfigurations(), URI.class).get();

                System.out.println("  [Component] " + component.getTitle() + " (" + configurationLocations.size() + " local configurations)");

                // loop on its configurations
                for (URI configurationLocation : configurationLocations) {
                    Configuration configuration = new GetConfiguration(client, configurationLocation).get();
                    printConfiguration(configuration);
                }
            }
        }

        private ServiceProvider loadConfigProvider(OslcClient client, RootServices rootServices) {

            // load the OSLC Config catalog
            ServiceProviderCatalog configCatalog = new GetServiceProviderCatalog(client, rootServices.getConfigCatalog()).get();

            // get the unique OSLC provider defined in there
            if (configCatalog.getServiceProviders().length != 1) {
                throw new RuntimeException("A unique service provider was expected in " + rootServices.getConfigCatalog());
            }

            // load the OSLC config provider
            return new GetServiceProvider(client, configCatalog.getServiceProviders()[0].getAbout()).get();
        }

        private Collection<Component> loadComponents(OslcClient client, ServiceProvider configProvider) {

            // lookup the component creation factory
            for (Service service : configProvider.getServices()) {
                CreationFactory factory = OslcCore.Finder.forCreationFactories(service).resourceType(URI.create(OslcConfig.TYPE_COMPONENT))
                        .findFirst();
                if (factory != null) {

                    // ensure this is the factory for the project we are looking for
                    if (rmProvider.getTitle().equals(factory.getTitle())) {

                        // Note: components are retrieved by sending a GET on the creation factory
                        return loadComponents(client, factory.getCreation());
                    }
                }
            }

            throw new RuntimeException("A creation factory for components was expected in " + configProvider.getAbout());
        }

        private Collection<Component> loadComponents(OslcClient client, URI uri) {
            Collection<Component> components = new GetResources<>(client, uri, Component.class).get();

            // load each Component to get the details
            Collection<Component> result = new ArrayList<>();
            for (Component component : components) {
                result.add(new GetComponent(client, component.getAbout()).get());
            }
            return result;
        }
    }

    /*
     * Prints the GC projects and their configurations
     */
    private static class ListGlobalConfigurations extends AbstractSnippet {

        private final RootServices rmRootServices;

        private ListGlobalConfigurations(RootServices rmRootServices) {
            this.rmRootServices = rmRootServices;
        }

        @Override
        protected void run(OslcClient client) {
            System.out.println("Loading global configurations...");

            // determine global configuration friend
            Optional<Friend> friend = new GetGlobalConfigurationFriend(client, rmRootServices.getAbout()).call();
            if (!friend.isPresent()) {
                System.out.println("The RM application has no GC friend application");
                return;
            }

            // load GC root services
            RootServices gcRootServices = new GetRootServices<>(friend.get().getRootServices(), RootServices.class).call();

            // load the OSLC Config catalog
            ServiceProviderCatalog configCatalog = new GetServiceProviderCatalog(client, gcRootServices.getConfigCatalog()).get();

            // loop on GC projects
            for (ServiceProvider provider : configCatalog.getServiceProviders()) {

                // load the full description of the provider if it doesn't contain services at this stage
                if (provider.getServices().length == 0) {
                    provider = new GetServiceProvider(client, provider.getAbout()).get();
                }

                System.out.println("  [Project] " + provider.getTitle());

                // determine the query to obtain configurations
                for (Service service : provider.getServices()) {
                    QueryCapability query = OslcCore.Finder.forQueryCapabilities(service).resourceType(URI.create(OslcConfig.TYPE_CONFIGURATION))
                            .findFirst();
                    if (query != null) {

                        // load configurations
                        Collection<Configuration> configurations = new GetResources<>(client, query.getQueryBase(), Configuration.class).get();
                        for (Configuration configuration : configurations) {
                            printConfiguration(configuration);
                        }
                    }
                }
            }
        }
    }
}
