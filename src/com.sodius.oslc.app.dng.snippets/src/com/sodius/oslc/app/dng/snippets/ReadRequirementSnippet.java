package com.sodius.oslc.app.dng.snippets;

import java.net.URI;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.Property;
import org.eclipse.lyo.oslc4j.core.model.ResourceShape;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import com.google.common.base.Strings;
import com.sodius.oslc.app.dng.model.AttributeType;
import com.sodius.oslc.app.dng.model.DngRequirement;
import com.sodius.oslc.app.dng.model.EnumEntry;
import com.sodius.oslc.app.dng.requests.GetAttributeType;
import com.sodius.oslc.app.dng.requests.GetRequirement;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResourceShape;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.OslcRm;

/**
 * Reads the specified DOORS Next requirement and prints out its properties, discovering them by inspecting its resource shape.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.requirement</code> - URL of the DOORS Next requirement,
 * which is obtained by right-clicking "Share Link to Artifact.." on a requirement in DOORS Next..</li>
 * </ul>
 */
public class ReadRequirementSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ReadRequirementSnippet().call();
    }

    private final Map<Property, AttributeType> customAtributeTypes;

    private ReadRequirementSnippet() {
        this.customAtributeTypes = new HashMap<>();
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // load requirement
        System.out.println("Reading requirement...");
        URI requirementUri = URI.create(getRequiredProperty("dng.requirement"));
        URI configuration = OslcConfig.getContext(requirementUri);
        DngRequirement requirement = new GetRequirement(client, requirementUri).get();

        // load resource shape
        System.out.println("Reading requirement type...");
        URI shapeUri = configuration == null ? requirement.getInstanceShape() : OslcConfig.addContext(requirement.getInstanceShape(), configuration);
        ResourceShape shape = new GetResourceShape(client, shapeUri).get();

        // parse custom attribute types
        for (Property property : shape.getProperties()) {
            if (isCustomAttributeType(requirement, property)) {
                URI typeUri = property.getRange()[0];
                if (configuration != null) {
                    typeUri = OslcConfig.addContext(typeUri, configuration);
                }
                AttributeType type = new GetAttributeType(client, typeUri).call().getEntity();
                this.customAtributeTypes.put(property, type);
            }
        }

        // display properties
        System.out.println("Requirement properties:");
        List<Property> properties = getSortedProperties(shape);
        for (Property property : properties) {
            System.out.println();
            print(requirement, property);
        }
    }

    /*
     * Determines if the property uses a custom DNG attribute type,
     * which is the case if the property defines a single oslc:range references an URI like
     * "https://server:9443/rm/types/_NiKp4T5PEeiKR4mfVe2q2Q"
     */
    private static boolean isCustomAttributeType(DngRequirement requirement, Property property) {
        if ((property.getRange().length == 1) && property.getRange()[0].toString().contains("/types/")) {
            URI requirementLocation = requirement.getAbout();
            URI propertyDefinition = property.getPropertyDefinition();

            return requirementLocation.getScheme().equals(propertyDefinition.getScheme())
                    && requirementLocation.getHost().equals(propertyDefinition.getHost())
                    && (requirementLocation.getPort() == propertyDefinition.getPort());
        }

        return false;
    }

    private void print(DngRequirement requirement, Property property) {

        // title
        System.out.println(property.getTitle() + " <" + property.getPropertyDefinition() + '>');

        // type
        System.out.println("  type:  " + getDisplayType(property));

        // custom type?
        AttributeType type = null;
        if (isCustomAttributeType(requirement, property)) {
            System.out.println("  custom: true");
            type = this.customAtributeTypes.get(property);

            // enum?
            if (type.getEnumEntries().length != 0) {
                System.out.println("  literals:");
                for (EnumEntry literal : type.getEnumEntries()) {
                    System.out.println("    [" + literal.getTitle() + "] <" + literal.getAbout() + '>');
                }
            }
        }

        // value
        printValue(requirement, property, type);
    }

    private void printValue(DngRequirement requirement, Property property, AttributeType type) {

        // resource property?
        if (isResource(property)) {
            Collection<Link> links = ResourceProperties.getLinks(requirement, property);

            // a single link is expected?
            if (isOccurs(property, Occurs.ZeroOrOne) || isOccurs(property, Occurs.ExactlyOne)) {
                if (links.isEmpty()) {
                    System.out.println("  link:  <none>");
                } else {
                    System.out.println("  link:  " + getDisplayLink(links.iterator().next(), type));
                }
            }

            // many links
            else {
                if (links.isEmpty()) {
                    System.out.println("  links: <none>");
                } else {
                    System.out.println("  links:");
                    for (Link link : links) {
                        System.out.println("    - " + getDisplayLink(link, type));
                    }
                }
            }
        }

        // primitive property
        else {
            // Note: use getObject() as, for a print out, we don't care whether the primitive is a String, a date or anything else.
            Object value = ResourceProperties.getObject(requirement, property);
            if (value == null) {
                System.out.println("  value: <none>");
            } else {
                System.out.println("  value: " + value);
            }
        }
    }

    private static List<Property> getSortedProperties(ResourceShape shape) {
        List<Property> properties = new ArrayList<>();
        for (Property property : shape.getProperties()) {

            // some properties have no title (projectArea, component, instanceShape) and are generally not of interest here
            if (!Strings.isNullOrEmpty(property.getTitle())) {
                properties.add(property);
            }
        }

        Collections.sort(properties, new Comparator<Property>() {
            @Override
            public int compare(Property o1, Property o2) {

                // put primitive properties before links
                if (isResource(o1)) {
                    if (!isResource(o2)) {
                        return 1;
                    }
                } else if (isResource(o2)) {
                    return -1;
                }

                // compare titles
                return Collator.getInstance().compare(o1.getTitle(), o2.getTitle());
            }
        });

        return properties;
    }

    private static String getDisplayType(Property property) {
        StringBuilder buffer = new StringBuilder();

        // type name
        buffer.append(getDisplayTypeName(property));

        // occurs
        String occurs = getDisplayOccurs(property);
        if (!Strings.isNullOrEmpty(occurs)) {
            buffer.append(' ');
            buffer.append(occurs);
        }

        return buffer.toString();
    }

    private static String getDisplayTypeName(Property property) {
        if (isType(property, ValueType.Boolean)) {
            return "boolean";
        } else if (isType(property, ValueType.Date)) {
            return "date";
        } else if (isType(property, ValueType.DateTime)) {
            return "datetime";
        } else if (isType(property, ValueType.Decimal)) {
            return "decimal";
        } else if (isType(property, ValueType.Double)) {
            return "double";
        } else if (isType(property, ValueType.Float)) {
            return "float";
        } else if (isType(property, ValueType.Integer)) {
            return "integer";
        } else if (isType(property, ValueType.String)) {
            return "string";
        } else if (isType(property, ValueType.XMLLiteral)) {
            return "xml literal";
        } else if (isResource(property)) {
            return getDisplayResourceRange(property);
        } else if (property.getValueType() == null) {
            return "<none>";
        } else {
            return property.getValueType().toString();
        }
    }

    private static String getDisplayResourceRange(Property property) {
        boolean isRequirement = false;
        boolean isRequirementCollection = false;

        URI[] ranges = property.getRange();
        if ((ranges != null) && (ranges.length != 0)) {
            for (URI uri : ranges) {
                isRequirement = isRequirement || OslcRm.TYPE_REQUIREMENT.equals(uri.toString());
                isRequirementCollection = isRequirementCollection || OslcRm.TYPE_REQUIREMENT_COLLECTION.equals(uri.toString());
            }
        }

        if (isRequirement || isRequirementCollection) {
            return "requirement/collection/module";
        } else {
            return "resource";
        }
    }

    /*
     * Note: some properties are not typed "Resource" but have a representation URI (inline or reference),
     * which indicates they are targeting a resource.
     */
    private static boolean isResource(Property property) {
        return isType(property, ValueType.Resource) || (property.getRepresentation() != null);
    }

    private static boolean isType(Property property, ValueType type) {
        return type.toString().equals(String.valueOf(property.getValueType()));
    }

    private static String getDisplayOccurs(Property property) {
        if (isOccurs(property, Occurs.ZeroOrMany)) {
            return "[0..*]";
        } else if (isOccurs(property, Occurs.OneOrMany)) {
            return "[1..*]";
        } else {
            return "";
        }
    }

    private static boolean isOccurs(Property property, Occurs occurs) {
        return occurs.toString().equals(String.valueOf(property.getOccurs()));
    }

    private static String getDisplayLink(Link link, AttributeType type) {
        String label = link.getLabel();

        if (Strings.isNullOrEmpty(label) && (type != null)) {
            label = "[" + type.getEnumEntry(link.getValue()).getTitle() + ']';
        }

        if (Strings.isNullOrEmpty(label)) {
            return "<" + link.getValue() + ">";
        } else {
            return label + " <" + link.getValue() + ">";
        }
    }

}
