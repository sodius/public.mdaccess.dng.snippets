package com.sodius.oslc.app.dng.snippets;

import java.io.File;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Scanner;

import javax.ws.rs.core.HttpHeaders;

import org.eclipse.lyo.oslc4j.core.model.CreationFactory;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.dng.model.DngRequirement;
import com.sodius.oslc.app.dng.model.Folder;
import com.sodius.oslc.app.dng.model.Module;
import com.sodius.oslc.app.dng.requests.CreateModule;
import com.sodius.oslc.app.dng.requests.CreateRequirement;
import com.sodius.oslc.app.dng.requests.CreateWrapperResource;
import com.sodius.oslc.app.dng.requests.GetFolder;
import com.sodius.oslc.app.dng.requests.GetModule;
import com.sodius.oslc.app.dng.requests.GetModuleService;
import com.sodius.oslc.app.dng.requests.GetRequirement;
import com.sodius.oslc.app.dng.requests.InsertRequirement;
import com.sodius.oslc.app.dng.requests.InsertRequirement.InsertLocation;
import com.sodius.oslc.app.dng.requests.SetRequirementHeading;
import com.sodius.oslc.app.dng.snippets.utils.Folders;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.client.requests.ResourceResponse;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.OslcRm;

/**
 * Create a new module within DOORS Next.
 *
 * <p>
 * Required Program arguments:
 * </p>
 * <ul>
 * <li><code>args[0]</code>: new title to assign to the DNG module</li>
 * </ul>
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>dng.user</code> - user ID to connect to DOORS Next.</li>
 * <li><code>dng.password</code> - password of the specified DOORS Next user.</li>
 * <li><code>dng.folder</code> - URL of the DOORS Next folder where to create a module, which can be obtained by executing ListFoldersSnippet.
 * The folder URI is also available by right-clicking "Share Link to Folder.." on a folder in DOORS Next.
 * </li>
 * </ul>
 */
public class CreateModuleSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Expects a module title as command line argument");
        }
        String title = args[0];
        new CreateModuleSnippet(title).call();
    }

    private final String title;

    public CreateModuleSnippet(String title) {
        this.title = title;
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // read folder
        System.out.println("Reading Folder...");
        URI folderLocation = Folders.format(URI.create(getRequiredProperty("dng.folder")));
        URI configuration = OslcConfig.getContext(folderLocation);
        Folder folder = new GetFolder(client, folderLocation).get();

        // loading project
        System.out.println("Reading Project...");
        URI providerLocation = configuration == null ? folder.getServiceProvider()
                : OslcConfig.addContext(folder.getServiceProvider(), configuration);
        ServiceProvider provider = new GetServiceProvider(client, folder.getServiceProvider()).get();

        System.out.println("Project: " + provider.getTitle() + " <" + providerLocation + '>');
        System.out.println("Folder: " + folder.getTitle() + " <" + folderLocation + '>');

        // list the module creation factories
        System.out.println("Querying module creation factories...");
        Service service = new GetModuleService(client, providerLocation).call();

        int factoriesCount = service.getCreationFactories().length;

        // no factory?
        if (factoriesCount == 0) {
            System.out.println("No module creation factory available.");
        }

        // only one?
        else if (factoriesCount == 1) {
            CreationFactory factory = service.getCreationFactories()[0];
            run(client, folderLocation, providerLocation, factory);
        }

        // user needs to choose one
        else {
            System.out.println(service.getCreationFactories().length + " available factories:");
            for (int i = 0; i < service.getCreationFactories().length; i++) {
                CreationFactory factory = service.getCreationFactories()[i];
                System.out.println("- [" + (i + 1) + "] " + factory.getTitle());
            }

            // waiting for the user to make a factory selection
            System.out.println("Which factory shall be used to create a module?");
            @SuppressWarnings("resource")
            int choice = new Scanner(System.in, Charset.defaultCharset().name()).nextInt();

            // create a module
            CreationFactory factory = service.getCreationFactories()[choice - 1];
            run(client, folderLocation, providerLocation, factory);
        }
    }

    private void run(OslcClient client, URI folderLocation, URI providerLocation, CreationFactory factory) throws Exception {

        // create module
        System.out.println("Creating " + factory.getTitle() + " module: " + title);
        URI moduleLocation = createModule(client, title, folderLocation, factory);
        moduleLocation = OslcConfig.addContext(moduleLocation, OslcConfig.getContext(folderLocation));

        System.out.println("Module was created at this location:");
        System.out.println(moduleLocation);

        // get module
        Module module = new GetModule(client, moduleLocation).get();

        // create requirements
        new CreateRequirementInModule(client, providerLocation, module).create();
    }

    private URI createModule(OslcClient client, String title, URI folderLocation, CreationFactory creationFactory) {
        Module module = new Module();
        module.setTitle(title);
        module.setParentFolder(OslcConfig.removeContext(folderLocation));
        module.setInstanceShape(creationFactory.getResourceShapes()[0]);

        URI uri = OslcConfig.addContext(creationFactory.getCreation(), OslcConfig.getContext(folderLocation));
        ResourceResponse<Void> response = new CreateModule(client, uri, module).call();
        return URI.create(response.getHeaders().getFirst(HttpHeaders.LOCATION));
    }

    private static class CreateRequirementInModule {

        private final OslcClient client;
        private final URI providerLocation;
        private final Module module;
        private final URI configuration;
        private URI folderLocation;
        private CreationFactory requirementCreationFactory;

        public CreateRequirementInModule(OslcClient client, URI providerLocation, Module module) {
            this.client = client;
            this.providerLocation = providerLocation;
            this.module = module;
            this.configuration = OslcConfig.getContext(providerLocation);
        }

        public void create() {
            // lookup requirement creation factory
            System.out.println("Querying requirement creation factory...");
            ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();
            Service service = OslcCore.Finder.forServices(provider).domain(URI.create(OslcRm.DOMAIN)).findFirst();
            requirementCreationFactory = OslcCore.Finder.forCreationFactories(service).resourceType(URI.create(OslcRm.TYPE_REQUIREMENT)).findFirst();

            // determine folder where module requirements shall be created (as indicated by the module)
            folderLocation = OslcConfig.addContext(module.getAssetFolder(), configuration);

            System.out.println("Creating requirements...");

            // create heading requirement in asset folder
            String primaryText = "<div xmlns=\"http://www.w3.org/1999/xhtml\">My Heading</div>";
            URI headingLocation = createRequirement("My Heading", primaryText, null);
            ResourceResponse<DngRequirement> response = new GetRequirement(client, headingLocation).call();
            String eTag = response.getETag();
            new SetRequirementHeading(client, headingLocation, eTag, true).call();

            // create simple requirement
            primaryText = "<div xmlns=\"http://www.w3.org/1999/xhtml\">This is the text of requirement generated by MDAccess for DOORS Next</div>";
            createRequirement("Simple Requirement", primaryText, headingLocation);

            // create requirement with formated text
            primaryText = "<div xmlns=\"http://www.w3.org/1999/xhtml\">This is the text of requirement <u>generated by <b>MDAccess for DOORS Next</b></u></div>";
            createRequirement("Formated Requirement", primaryText, headingLocation);

            // create requirement with HTML table
            primaryText = "<div xmlns=\"http://www.w3.org/1999/xhtml\">"
                    + "<table cellspacing=\"1\" cellpadding=\"1\" style=\"overflow: hidden; width: 100px; border: 1px solid black;\"><tbody><tr>"
                    + "<td style=\"border: 1px solid black; \"><p>1</p></td><td style=\"border: 1px solid black; \"><p>2</p></td>"
                    + "<td style=\"border: 1px solid black; \"><p>3</p></td></tr><tr>"
                    + "<td style=\"border: 1px solid black; \"><p>4</p></td><td style=\"border: 1px solid black; \"><p>5</p></td>"
                    + "<td style=\"border: 1px solid black; \"><p>6</p></td></tr></tbody></table></div>";
            createRequirement("Table Requirement", primaryText, headingLocation);

            // create requirement with an image as attachment
            DngRequirement imageWrapper = createAttachment("Image", "resources/image.png", "image/png");
            primaryText = "<div xmlns=\"http://www.w3.org/1999/xhtml\"><img alt=\"Embedded image\" src=\""
                    + OslcConfig.addContext(imageWrapper.getWrappedResource(), configuration) + "\" style=\"height:150px\"></img></div>";
            createRequirement("My image", primaryText, headingLocation);

            System.out.println("Done.");
        }

        private URI createRequirement(String title, String primaryText, URI parent) {
            DngRequirement requirement = new DngRequirement();
            requirement.setTitle(title);
            requirement.setPrimaryText(primaryText);
            requirement.setParentFolder(OslcConfig.removeContext(folderLocation));
            requirement.setInstanceShape(requirementCreationFactory.getResourceShapes()[0]);

            // create base artifact
            URI uri = OslcConfig.addContext(requirementCreationFactory.getCreation(), configuration);
            ResourceResponse<Void> response = new CreateRequirement(client, uri, requirement).call();
            URI baseArtifactLocation = URI.create(response.getHeaders().getFirst(HttpHeaders.LOCATION));

            InsertLocation location;
            if (parent != null) {
                // insert requirement under parent if present
                location = InsertLocation.below(parent).build();
            } else {
                // insert requirement at module root
                location = InsertLocation.root(module).build();
            }
            ResourceResponse<Void> insertResponse = new InsertRequirement(client, OslcConfig.addContext(baseArtifactLocation, configuration),
                    location).call();
            URI requirementLocation = URI.create(insertResponse.getHeaders().getFirst(HttpHeaders.LOCATION));
            URI versionedRequirement = OslcConfig.addContext(requirementLocation, configuration);
            return versionedRequirement;
        }

        private DngRequirement createAttachment(String title, String filePath, String contentType) {
            // We instantiate a wrapper resource with a file to upload
            DngRequirement wrapper = new DngRequirement();
            wrapper.setParentFolder(OslcConfig.removeContext(folderLocation));
            wrapper.setTitle(title);
            wrapper.setDescription("My attachment");
            wrapper.setInstanceShape(requirementCreationFactory.getResourceShapes()[0]);
            wrapper.setWrappedResourceContentType(contentType);
            File file = new File(filePath);

            // We create the wrapper resource in DNG
            System.out.println("Creating wrapper resource...");
            ResourceResponse<Void> wrapperResponse = new CreateWrapperResource(client,
                    OslcConfig.addContext(requirementCreationFactory.getCreation(), configuration), wrapper, file).call();
            wrapper.setAbout(URI.create(wrapperResponse.getHeaders().getFirst(HttpHeaders.LOCATION)));
            wrapper = new GetRequirement(client, OslcConfig.addContext(wrapper.getAbout(), configuration)).call().getEntity();

            return wrapper;
        }

    }
}
